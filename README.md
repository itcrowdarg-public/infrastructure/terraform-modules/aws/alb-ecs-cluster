# ALB - ECS Cluster

## Usage

```terraform
module "load_balancer" {
  source   = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/alb-ecs-cluster.git?ref=1.0"

  vpc          = module.vpc
  environments = var.environments
  region       = var.region

}
```