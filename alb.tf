resource "aws_lb" "alb" {
  for_each           = { for i, v in var.environments : i => v }
  name               = "alb-${each.value.name}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_allow_http_internet.id]
  subnets            = var.vpc.public_subnets

  enable_deletion_protection = false
}
