resource "aws_alb_listener" "https" {
  for_each          = { for i, v in var.environments : i => v }
  load_balancer_arn = aws_lb.alb[each.key].id
  port              = "443"
  protocol          = "HTTPS"

  # If each.value.alb_certificate_arn is empty string (""), then use the self-signed certificate
  certificate_arn = each.value.alb_certificate_arn != "" ? each.value.alb_certificate_arn : aws_acm_certificate.cert.arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content [https] - Terraform"
      status_code  = "200"
    }
  }
}
resource "aws_alb_listener" "http" {
  for_each          = { for i, v in var.environments : i => v }
  load_balancer_arn = aws_lb.alb[each.key].id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    # type = "fixed-response"
    # fixed_response {
    #   content_type = "text/plain"
    #   message_body = "Fixed response content [http] - Terraform"
    #   status_code  = "200"

    # }
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
