output "alb" {
  value = aws_lb.alb
}


output "listener_https" {
  value = aws_alb_listener.https
}

output "security_group_alb_allow_web_id" {
  value = aws_security_group.alb_allow_http_internet.id
}
