variable "vpc" {
  description = "VPC where the ASG is created"
}

variable "environments" {
  description = "List of available environments"
}

variable "region" {
  description = "AWS region"
  type        = string
}